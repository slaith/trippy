class User 
  include Neo4j::ActiveNode
  property :user_id, type: String
  has_many :out,:followed, type: :FOLLOWING, :model_class=>:User
  has_many :in, :followers,type: :FOLLOWING, :model_class=>:User
  has_many :out, :liked, type: :LIKES, :model_class=>:Trip
  has_many :out, :wished, type: :WISHES, :model_class=>:Trip

  ## Tries to find a user with the given Id
  # If he doesn't exist, creates a new user

  def self.find_or_make(uid)
  	 	puts "Searching #{uid}"
  	 	uid=uid.to_s
  	  user=self.where("user_id"=>uid).first
  	  puts "#{user.inspect}"
  	  if(user.blank?)
  	  	puts "User is blank. Creating new one"
  	  	user=User.new
  		user.user_id=uid
  		user.save
  		puts "Successfully created user #{user.inspect}"

  	  end
  	  puts
  	  return user

  end


end
