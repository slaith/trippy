
module ApplicationHelper

  ## Checks if the string is a number
  def is_number?(str)
    true if Float(str) rescue false
  end
end
