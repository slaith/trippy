require "#{Rails.root}/app/helpers/application_helper"

include ApplicationHelper
namespace :import do
  desc "TODO"
  task :logs,[:directory] =>:environment do | t,args|
  	directory=args[:directory]
  	files= Dir[directory+"/*"]
  	files.each do |filepath|
  		file=File.new(filepath,"r")
      puts "Parsing file #{filepath}"
  		## Iterates over the file line by line. Each line is a json entry
  		while(line=file.gets)
        puts "--Parsing line ..."

  			data=JSON.parse line
        puts data["user_id"]
  			##Checks if the user id is numeric
  			if(is_number?(data["user_id"]))
          puts "-- -- User id is numeric. Continue"
  				user=User.find_or_make(data["user_id"])
          
          ## If there is a body in the first place, proceed and switch on action
          if(data["body"]["action"])
            action=data["body"]["action"]
            #user.followed=user.followed||[]
            case action  
              when "follow"
                followed=data["body"]["followed_id"]
                #If the followed_id is an array, we process each element
                if (followed.is_a? Array)
                  puts followed.inspect
                  followed.each do |uid|
                    
                    followed_user=User.find_or_make(uid)
                    if(!user.followed.include? followed_user)
                      user.followed<<followed_user
                    end
                  end#followed.each
                # If its a string, we simply process the eleemnt itself
                elsif(followed.is_a? String)
                   
                    followed_user=User.find_or_make(Integer(followed))
                    
                    if(!user.followed.include? followed_user )
                      user.followed<<followed_user
                    end

                end#if(is_a)
                user.save
              when "like"
                puts "GETS to Like"

                liked=data["body"]["target_id"]
                target_type=data["body"]["target_type"]
                case target_type
                  when "trip"
                    puts "GETS to TRip"
                    trip=Trip.find_or_make(liked)
                    if(!user.liked.include? trip)
                      user.liked<<trip
                    end
                  when "user"
                    user=User.find_or_make(liked)
                    if(!user.liked.include? trip)
                      user.liked<<user
                    end
                end#case target_type


            end # case action

          end # if body



  			end # Is number 


  		end #While line

  	end # files.each 

  end # task

end # namespace
